/***************************************************************************
 *
 *            (C) Copyright 2010 The Board of Trustees of the
 *                        University of Illinois
 *                         All Rights Reserved
 *
 ***************************************************************************/

#include <cuda.h>

#include "util.h"

struct Block{
    int ID;
    clock_t clk;
};
const int M = 8;
volatile __device__ Block TimeAr[M];
volatile __device__ int CurrThreadBlocks[M];

__device__ clock_t get_clock(void)
{
    clock_t clock;
    asm("mov.u32 %0, %clock;" : "=r"(clock) );
    return clock;
}

__device__ void Compute_ExecTime(clock_t clk, int blockID, double* ExecTime)
{
    for(int i=0;i<M;i++)
        Asm("declare i32 @llvm.nvvm.read.ptx.sreg.ctaid.x(i)" : "=r"(CurrThreadBlocks[i]));
    for(int index=0;index<M;index++)
    {
        bool flag = 0;
        for(int j=0;j<M;j++)
            if(CurrThreadBlocks[j]==TimeAr[index].ID)
            {
                flag = 1;
                break;
            }
        if(flag==0)
        {
            ExecTime[TimeAr[index].ID] = (double)(clk-TimeAr[index].clk)/CLOCKS_PER_SEC;
            TimeAr[index].clk = clk;
            TimeAr[index].ID = blockID;
            return;
        }
    }
}
__device__ void calculateBin (
        const unsigned int bin,
        uchar4 *sm_mapping)
{
        unsigned char offset  =  bin        %   4;
        unsigned char indexlo = (bin >>  2) % 256;
        unsigned char indexhi = (bin >> 10) %  KB;
        unsigned char block   =  bin / BINS_PER_BLOCK;

        offset *= 8;

        uchar4 sm;
        sm.x = block;
        sm.y = indexhi;
        sm.z = indexlo;
        sm.w = offset;

        *sm_mapping = sm;
}

__global__ void histo_intermediates_kernel (
        uint2 *input,
        unsigned int height,
        unsigned int width,
        unsigned int input_pitch,
        uchar4 *sm_mappings,
		double *ExecTime)
{
	if(threadIdx.x==0)
	{
		clock_t clk = get_clock();
		if(blockIdx.x<M)
		{
			TimeAr[blockIdx.x].clk = clk;
			TimeAr[blockIdx.x].ID = blockIdx.x;
		}
		else
			Compute_ExecTime(clk, blockIdx.x,ExecTime);
	}
	else if ((Nk-blockIdx.x)<=M && threadIdx.x==MAX_THREADS_PER_BLOCK-1)
	{
		clock_t clk = get_clock();
		int index;
		for(index=0;index<M;index++)
		{
			if(blockIdx.x==TimeAr[index].ID)
				break;
		}
		ExecTime[blockIdx.x] = (double)(clk-TimeAr[index].clk)/CLOCKS_PER_SEC;
	}
        unsigned int line = UNROLL * blockIdx.x;// 16 is the unroll factor;

        uint2 *load_bin = input + line * input_pitch + threadIdx.x;

        unsigned int store = line * width + threadIdx.x;
        bool skip = (width % 2) && (threadIdx.x == (blockDim.x - 1));

        #pragma unroll
        for (int i = 0; i < UNROLL; i++)
        {
                uint2 bin_value = *load_bin;

                calculateBin (
                        bin_value.x,
                        &sm_mappings[store]
                );

                if (!skip) calculateBin (
                        bin_value.y,
                        &sm_mappings[store + blockDim.x]
                );

                load_bin += input_pitch;
                store += width;
        }
}
