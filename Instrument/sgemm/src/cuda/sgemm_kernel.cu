/***************************************************************************
 *cr
 *cr            (C) Copyright 2010 The Board of Trustees of the
 *cr                        University of Illinois
 *cr                         All Rights Reserved
 *cr
 ***************************************************************************/

/* 
 * Kernel of dense matrix-matrix multiplication kernel.
 * The algorithm is based on CUDA sgemm code from Vasily Volkov
 * at UC Berkeley.
 */

#include <stdio.h>
#include <sys/time.h>
#include <time.h>
#include <stdint.h>
#include <cooperative_groups.h>

struct Block{
    int ID;
    clock_t clk;
};
const int M = 4;
const int Nk = 15775;
volatile __device__ Block TimeAr[M];
volatile __device__ int CurrThreadBlocks[M];
#define MAX_THREADS_PER_BLOCK 128

__device__ clock_t get_clock(void)
{
    clock_t clock;
    asm("mov.u32 %0, %clock;" : "=r"(clock) );
    return clock;
}

__device__ void Compute_ExecTime(clock_t clk, int blockID, double* ExecTime)
{
    for(int i=0;i<M;i++)
        Asm("declare i32 @llvm.nvvm.read.ptx.sreg.ctaid.x(i)" : "=r"(CurrThreadBlocks[i]));
    for(int index=0;index<M;index++)
    {
        bool flag = 0;
        for(int j=0;j<M;j++)
            if(CurrThreadBlocks[j]==TimeAr[index].ID)
            {
                flag = 1;
                break;
            }
        if(flag==0)
        {
            ExecTime[TimeAr[index].ID] = (double)(clk-TimeAr[index].clk)/CLOCKS_PER_SEC;
            TimeAr[index].clk = clk;
            TimeAr[index].ID = blockID;
            return;
        }
    }
}

#define CHECK_ERROR(errorMessage) {                                    \
  cudaError_t err = cudaGetLastError();                                    \
  if( cudaSuccess != err) {                                                \
    fprintf(stderr, "Cuda error: %s in file '%s' in line %i : %s.\n",    \
	errorMessage, __FILE__, __LINE__, cudaGetErrorString( err) );\
    exit(EXIT_FAILURE);                                                  \
  }                                                                        \
}

// CML x RML = CML, baseline version, 510FLOP/s on Fermi
/* Pseudo code
for i < M ; i += 64   // thread block.x
 for j < N; j += 16   // thread block.y
  for tx = 0; tx < 16; tx++ // thread index x; tile of M loop
  for ty = 0; ty < 4 ; ty++ // thread index y; tile of M loop

  for m < 16; m += 1;
     c[m] = 0.0f

  for k < K; k += 4   // seq

   b[ty][tx] = B[k+ty][j+tx]

   for l < 4; l +=1   // seq
    for m < 16; m +=1 // seq
      c[m] += A[i+ty*16+tx][k+l]+b[l][m]

*/

// Parameters of tile sizes
#define TILE_N 16 
#define TILE_TB_HEIGHT 8
#define TILE_M (TILE_N*TILE_TB_HEIGHT)

__global__ void mysgemmNT( const float *A, int lda, const float *B, int ldb, float* C, int ldc, int k, float alpha, float beta, double* ExecTime )
{
    if(threadIdx.x==0)
    {
        clock_t clk = get_clock();
        if(blockIdx.x<M)
        {
            TimeAr[blockIdx.x].clk = clk;
            TimeAr[blockIdx.x].ID = blockIdx.x;
        }
        else
            Compute_ExecTime(clk, blockIdx.x,ExecTime);
    }
    else if ((Nk-blockIdx.x)<=M && threadIdx.x==MAX_THREADS_PER_BLOCK-1)
    {
        clock_t clk = get_clock();
        int index;
        for(index=0;index<M;index++)
        {
            if(blockIdx.x==TimeAr[index].ID)
                break;
        }
        ExecTime[blockIdx.x] = (double)(clk-TimeAr[index].clk)/CLOCKS_PER_SEC;
    }
    // Partial results 
    float c[TILE_N];
    for (int i=0; i < TILE_N; i++)
	c[i] = 0.0f;
    int mid = threadIdx.y * blockDim.x + threadIdx.x; //flattened id
    int m = blockIdx.x * TILE_M + mid;
    int n = blockIdx.y * TILE_N + threadIdx.x;
    __shared__ float b_s[TILE_TB_HEIGHT][TILE_N];
    for (int i = 0; i < k; i+=TILE_TB_HEIGHT) {
	float a; 
	b_s[threadIdx.y][threadIdx.x]=B[n + (i+threadIdx.y)*ldb];
	__syncthreads();
	for (int j = 0; j < TILE_TB_HEIGHT; j++) {
	    a = A[m + (i+j)*lda];
	    for (int kk = 0; kk < TILE_N; kk++)
		c[kk] += a * b_s[j][kk];

	}
	__syncthreads();
    }
    int t = ldc*blockIdx.y * TILE_N + m;
    for (int i = 0; i < TILE_N; i++) {
	C[t+i*ldc] = C[t+i*ldc] * beta + alpha * c[i];
    }
}

void regtileSgemm( char transa, char transb, int m, int n, int k, float alpha, const float *A, int lda, const float *B, int ldb, float beta, float *C, int ldc, double* ExecTime )
{
  if ((transa != 'N') && (transa != 'n')) {
    std::cerr << "unsupported value of 'transa' in regtileSgemm()" << std::endl;
    return;
  }
  
  if ((transb != 'T') && (transb != 't')) {
    std::cerr << "unsupported value of 'transb' in regtileSgemm()" << std::endl;
    return;
  }
  
  // In this code we assume the matrix sizes are multiple of tile size
  if ((m%TILE_M) || (n%TILE_N)) {
    std::cerr << "unsupported size of matrix. m should be multiple of " << TILE_M
      << "; n should be multiple of " << TILE_N << std::endl;
  }


  dim3 grid( m/TILE_M, n/TILE_N ), threads( TILE_N, TILE_TB_HEIGHT );
  mysgemmNT<<<grid, threads>>>( A, lda, B, ldb, C, ldc, k, alpha, beta, ExecTime);
  CHECK_ERROR("mySgemm");

}

