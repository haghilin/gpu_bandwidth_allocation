/***************************************************************************
 *cr
 *cr            (C) Copyright 2010 The Board of Trustees of the
 *cr                        University of Illinois
 *cr                         All Rights Reserved
 *cr
 ***************************************************************************/

#include <stdio.h>
#include <sys/time.h>
#include <time.h>
#include <stdint.h>
#include <cooperative_groups.h>

struct Block{
    int ID;
    clock_t clk;
};
const int M = 8;
const int Nk = 31952;
#define MAX_THREADS_PER_BLOCK 128
volatile __device__ Block TimeAr[M];
volatile __device__ int CurrThreadBlocks[M];
#ifndef _COMMON_H_
#define _COMMON_H_
#define Index3D(_nx,_ny,_i,_j,_k) ((_i)+_nx*((_j)+_ny*(_k)))
#endif
