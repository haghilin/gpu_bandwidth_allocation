#define WARP_BITS 5
#define WARP_SIZE 32



#include <stdio.h>
#include <sys/time.h>
#include <time.h>
#include <stdint.h>
#include <cooperative_groups.h>

struct Block{
    int ID;
    clock_t clk;
};
const int M = 4;
const int Nk = 31624;
int MAX_THREADS_PER_BLOCK;
volatile __device__ Block TimeAr[M];
volatile __device__ int CurrThreadBlocks[M];
__device__ clock_t get_clock(void)
{
    clock_t clock;
    asm("mov.u32 %0, %clock;" : "=r"(clock) );
    return clock;
}

__device__ void Compute_ExecTime(clock_t clk, int blockID, double* ExecTime)
{
    for(int i=0;i<M;i++)
        asm("declare i32 @llvm.nvvm.read.ptx.sreg.ctaid.x(i)" : "=r"(CurrThreadBlocks[i]));
    for(int index=0;index<M;index++)
    {
        bool flag = 0;
        for(int j=0;j<M;j++)
            if(CurrThreadBlocks[j]==TimeAr[index].ID)
            {
                flag = 1;
                break;
            }
        if(flag==0)
        {
            ExecTime[TimeAr[index].ID] = (double)(clk-TimeAr[index].clk)/CLOCKS_PER_SEC;
            TimeAr[index].clk = clk;
            TimeAr[index].ID = blockID;
            return;
        }
    }
}
__global__ void spmv_jds(float *dst_vector,
						const float *d_data,const int *d_index, const int *d_perm,
						const float *x_vec,const int *d_nzcnt,const int dim,double * ExecTime)
{
	if(threadIdx.x==0)
	{
		clock_t clk = get_clock();
		if(blockIdx.x<M)
		{
			TimeAr[blockIdx.x].clk = clk;
			TimeAr[blockIdx.x].ID = blockIdx.x;
		}
		else
			Compute_ExecTime(clk, blockIdx.x,ExecTime);
	}
	else if ((Nk-blockIdx.x)<=M && threadIdx.x==MAX_THREADS_PER_BLOCK-1)
	{
		clock_t clk = get_clock();
		int index;
		for(index=0;index<M;index++)
		{
			if(blockIdx.x==TimeAr[index].ID)
				break;
		}
		ExecTime[blockIdx.x] = (double)(clk-TimeAr[index].clk)/CLOCKS_PER_SEC;
	}
	int ix=blockIdx.x*blockDim.x+threadIdx.x;
	int warp_id=ix>>WARP_BITS;
	if(ix<dim)
	{
		float sum=0.0f;
		int	bound=sh_zcnt_int[warp_id];
		//prefetch 0
		int j=jds_ptr_int[0]+ix;  
		float d = d_data[j]; 
		int i = d_index[j];  
		float t = x_vec[i];
		
		if (bound>1)  //bound >=2
		{
			//prefetch 1
			j=jds_ptr_int[1]+ix;    
			i =  d_index[j];  
			int in;
			float dn;
			float tn;
			for(int k=2;k<bound;k++ )
			{	
				//prefetch k-1
				dn = d_data[j]; 
				//prefetch k
				j=jds_ptr_int[k]+ix;    
				in = d_index[j]; 
				//prefetch k-1
				tn = x_vec[i];
				
				//compute k-2
				sum += d*t; 
				//sweep to k
				i = in;  
				//sweep to k-1
				d = dn;
				t =tn; 
			}	
		
			//fetch last
			dn = d_data[j];
			tn = x_vec[i];
	
			//compute last-1
			sum += d*t; 
			//sweep to last
			d=dn;
			t=tn;
		}
		//compute last
		sum += d*t;  // 3 3
		
		//write out data
		dst_vector[d_perm[ix]]=sum; 
	}

}

