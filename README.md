This repository contains the instrumentation code and data collected from 
instrumentation for the Parboil benchmarks.

Due to space limitation, the data are in 3 different repositories, which is 
defined later.

There should be a million files for each benchmark that correspond to million 
runs.

Each benchmark data may be stored in "n" different files, which means there are 
1000000/n files.

For example, if we have "histo_Thread_Block_1" and "histo_Thread_Block_2", there
are 500000 files in each of them.

There is one file for cutcp, sgemm, and mri_q, and two files for mri_gridding, 
tpacf, sad, histo, lbm, spmv, and stencil, and four files for bfs.

The files for histo, spmv, stencil, and lbm can be found in:
https://git.uwaterloo.ca/rpellizz/gpu_bw_allocation.

The files for bfs and sad can be found in:
https://git.uwaterloo.ca/haghilin/gpu_bw_allocation_cont.
